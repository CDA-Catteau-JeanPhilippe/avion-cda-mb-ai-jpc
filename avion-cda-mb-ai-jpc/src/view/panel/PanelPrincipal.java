package view.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.sun.corba.se.impl.orb.ParserTable.TestAcceptor1;

import models.Models;
import view.objetspatial.MeteoriteDeFeu;
import view.objetspatial.Vaisseau;

public class PanelPrincipal extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	private boolean enPartie = true;
	private Thread animationVaisseau;
	private Thread animationMeteoriteDeFeu;
	private Vaisseau vaisseau;
	private MeteoriteDeFeu meteoriteDeFeu;

	public PanelPrincipal() {
		meteoriteDeFeu = new MeteoriteDeFeu(Models.getPositionXDeLaMeteoriteDeFeuAuDepart(),
				Models.getPositionYDeLaMeteoriteDeFeuAuDepart(), 1);
		vaisseau = new Vaisseau(Models.getPositionXDuVaisseauAuDepart(), Models.getPositionYDuVaisseauAuDepart(), 1);
		this.addKeyListener(new KeyAdapter() {

			public void keyReleased(KeyEvent e) {
				System.out.println("Key Pressed: " + e.getKeyCode());
				vaisseau.setMouvementBas(false);
				vaisseau.setMouvementDroite(false);
				vaisseau.setMouvementGauche(false);
				vaisseau.setMouvementHaut(false);
			}

			public void keyPressed(KeyEvent e) {
				System.out.println("Key Pressed: " + e.getKeyCode());
				int key = e.getKeyCode();

				if (key == 40 || key == 83) {
					vaisseau.setMouvementBas(true);
				}
				if (key == 39 || key == 68) {
					vaisseau.setMouvementDroite(true);
				}
				if (key == 38 || key == 90) {
					vaisseau.setMouvementHaut(true);
				}
				if (key == 37 || key == 81) {
					vaisseau.setMouvementGauche(true);
				}
			}
		});

		this.setFocusable(true);
		this.setBackground(Color.black);

		if (animationVaisseau == null || animationMeteoriteDeFeu == null || !enPartie) {
			animationVaisseau = new Thread(this);
			animationVaisseau.start();
			meteoriteDeFeu.setMouvementBas(true);
			animationMeteoriteDeFeu = new Thread(this);
			animationMeteoriteDeFeu.start();
		}

		this.setDoubleBuffered(true);
	}

	public void run() {
		int delaiDAnimation = 5;
		long temps = System.currentTimeMillis();
		while (true) {
			this.repaint();
			try {
				temps = temps + delaiDAnimation;
				Thread.sleep(Math.max(0, temps - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				System.out.println(e);
			}
		}

	}

	public void paint(Graphics g) {
		super.paint(g);
		// BackGround
		Image testImage = load("/image/background.jpg");
		g.drawImage(testImage, 0, 0, Models.getLargeurDuPanneau(), Models.getHauteurDuPanneau(), this);

		// Vaisseau
		if (vaisseau.isMouvementDroite() == true) {
			if (!(vaisseau.getPosX() + 140 == Models.getLargeurDuPanneau())) {
				vaisseau.setPosX(vaisseau.getPosX() + vaisseau.getVitesse());
			}
		}
		if (vaisseau.isMouvementGauche() == true) {
			if (!(vaisseau.getPosX() == 0)) {
				vaisseau.setPosX(vaisseau.getPosX() - vaisseau.getVitesse());
			}
		}
		if (vaisseau.isMouvementHaut() == true) {
			if (!(vaisseau.getPosY() == 0)) {
				vaisseau.setPosY(vaisseau.getPosY() - vaisseau.getVitesse());
			}
		}
		if (vaisseau.isMouvementBas() == true) {
			if (!(vaisseau.getPosY() + 110 == Models.getHauteurDuPanneau() - 30)) {
				vaisseau.setPosY(vaisseau.getPosY() + vaisseau.getVitesse());
			}
		}
		if (meteoriteDeFeu.isMouvementBas() == true) {
			Random r = new Random();
			meteoriteDeFeu.setPosX(r.nextInt(Models.getLargeurDuPanneau()));
			meteoriteDeFeu.setPosY(meteoriteDeFeu.getPosY() + 1);

			if (meteoriteDeFeu.getPosY() > Models.getHauteurDuPanneau()) {
				meteoriteDeFeu.setPosX(r.nextInt(Models.getLargeurDuPanneau()));
				meteoriteDeFeu.setPosX(0);
			}
		}
		if (enPartie) {
			try {
				Image testImage3 = load("/image/vaisseau.png");
				g.drawImage(testImage3, vaisseau.getPosX(), vaisseau.getPosY(), 140, 85, this);
				Image testImage4 = load("/image/meteorite_feu.png");
				g.drawImage(testImage4, meteoriteDeFeu.getPosX(), meteoriteDeFeu.getPosY(), 50, 50, this);
				Image testImage5 = load("/image/meteorite_glace.png");
				g.drawImage(testImage5, 50, 0, 50, 50, this);
				Image testImage6 = load("/image/meteorite_simple.png");
				g.drawImage(testImage6, 100, 0, 50, 50, this);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}

	public static Image load(String file) {
		try {
			return ImageIO.read(PanelPrincipal.class.getResourceAsStream(file));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}

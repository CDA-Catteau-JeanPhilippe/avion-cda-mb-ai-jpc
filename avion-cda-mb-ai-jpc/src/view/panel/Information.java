package view.panel;

import view.objetspatial.Vaisseau;

public class Information {
	private static String nomUtilisateur;
	private static int score = 0;
	private static int vie = Vaisseau.getVie();

	public static String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public static void setNomUtilisateur(String nomUtilisateur) {
		Information.nomUtilisateur = nomUtilisateur;
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Information.score = score;
	}

	public static int getVie() {
		return vie;
	}

	public static void setVie(int vie) {
		Information.vie = vie;
	}
	
}

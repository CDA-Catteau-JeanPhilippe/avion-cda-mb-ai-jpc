package view.panel;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FenetreCreationPartie extends JFrame {

	private static final long serialVersionUID = 1L;
	private static String nomUtilsateur = "default";
	
	public FenetreCreationPartie() {
		setNomUtilsateur(JOptionPane.showInputDialog("Bienvenue.\nIns�rez votre nom d'utilisateur :"));
		while(getNomUtilsateur().length() < 3 || getNomUtilsateur().length() > 6 || getNomUtilsateur().contains(";")) {
			JOptionPane.showMessageDialog(null,
					"Vous ne respectez pas les conditions du pseudo\nMinimum 3 caract�res\nMaximum 6 caract�res\nIl ne peux y avoir de ';'",
					"Erreur", JOptionPane.ERROR_MESSAGE);
			setNomUtilsateur(JOptionPane.showInputDialog("Essayez de nouveau :"));
		}
	}

	public static String getNomUtilsateur() {
		return nomUtilsateur;
	}

	public static void setNomUtilsateur(String nomUtilsateur) {
		FenetreCreationPartie.nomUtilsateur = nomUtilsateur;
	}
}

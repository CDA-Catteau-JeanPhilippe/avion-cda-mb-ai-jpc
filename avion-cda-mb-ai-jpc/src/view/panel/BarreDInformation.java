package view.panel;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import view.objetspatial.Vaisseau;

public class BarreDInformation extends JFrame{

	private static final long serialVersionUID = 1L;
	private JToolBar barreDInformation;
	
	public BarreDInformation() {
		Information.setNomUtilisateur(FenetreCreationPartie.getNomUtilsateur());
		barreDInformation = new JToolBar();
		getContentPane().add(barreDInformation, BorderLayout.NORTH);
		JTextField tf = new JTextField();
		tf.setText("Score : " + Information.getScore());
		tf.setEditable(false);
		tf.setFocusable(false);
		JTextField tf2 = new JTextField();
		System.out.println(Information.getNomUtilisateur());
		tf2.setText(Information.getNomUtilisateur());
		tf2.setEditable(false);
		tf2.setFocusable(false);
		JTextField tf3 = new JTextField();
		tf3.setText("Vie : " + Vaisseau.getVie());
		tf3.setEditable(false);
		tf3.setFocusable(false);
		barreDInformation.add(tf, BorderLayout.WEST);
		barreDInformation.add(tf2);
		barreDInformation.add(tf3, BorderLayout.EAST);
	}

	public JToolBar getBarreDInformation() {
		return barreDInformation;
	}

	public void setBarreDInformation(JToolBar barreDInformation) {
		this.barreDInformation = barreDInformation;
	}
	
	
}

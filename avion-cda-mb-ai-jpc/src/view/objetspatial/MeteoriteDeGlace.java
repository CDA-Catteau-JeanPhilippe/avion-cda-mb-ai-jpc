package view.objetspatial;

import models.Models;

public final class MeteoriteDeGlace extends ObjetSpatial{
	public MeteoriteDeGlace(int unePosX, int unePosY, int uneVitesse) {
		super(unePosX, unePosY, uneVitesse, Models.getPointMeteoriteDeGlace(), "/image/meteorite_glace.png");
	}
}

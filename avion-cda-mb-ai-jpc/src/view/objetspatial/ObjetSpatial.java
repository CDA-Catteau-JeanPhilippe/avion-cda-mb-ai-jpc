package view.objetspatial;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;

public abstract class ObjetSpatial {
	private Image monImage;
	private ImageIcon nomImageIcon;
	private String cheminImage;
	private int posX;
	private int posY;
	private int vitesse;
	private int score;
	private boolean mouvementBas;
	private boolean vivant;

	public ObjetSpatial(int unePosX, int unePosY, int uneVitesse, int unScore, String unCheminDImage) {
		this.posX = unePosX;
		this.posY = unePosY;
		this.vitesse = uneVitesse;
		this.score = unScore;
		this.mouvementBas = false;
		this.vivant = true;
		this.cheminImage = unCheminDImage;
		this.monImage = Toolkit.getDefaultToolkit().getImage(Vaisseau.class.getResource(this.getCheminImage()).getFile());
	}

	public Image getMonImage() {
		return monImage;
	}

	public void setMonImage(Image monImage) {
		this.monImage = monImage;
	}

	public ImageIcon getNomImageIcon() {
		return nomImageIcon;
	}

	public void setNomImageIcon(ImageIcon nomImageIcon) {
		this.nomImageIcon = nomImageIcon;
	}

	public String getCheminImage() {
		return cheminImage;
	}

	public void setCheminImage(String cheminImage) {
		this.cheminImage = cheminImage;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getVitesse() {
		return vitesse;
	}

	public void setVitesse(int vitesse) {
		this.vitesse = vitesse;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isMouvementBas() {
		return mouvementBas;
	}

	public void setMouvementBas(boolean mouvementBas) {
		this.mouvementBas = mouvementBas;
	}

	public boolean isVivant() {
		return vivant;
	}

	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}
}

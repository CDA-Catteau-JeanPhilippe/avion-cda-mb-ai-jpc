package view.objetspatial;

import java.awt.Image;
import java.util.Random;

import models.Models;

public class AfficheImageMeteoriteDeFeu{
	private Image img;
    
	public AfficheImageMeteoriteDeFeu(MeteoriteDeFeu meteoriteDeFeu) {
		this.setImg(meteoriteDeFeu.getMonImage());

		Random r = new Random();
		meteoriteDeFeu.setPosX(r.nextInt(Models.getLargeurDuPanneau()));

	}
	
	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}
}

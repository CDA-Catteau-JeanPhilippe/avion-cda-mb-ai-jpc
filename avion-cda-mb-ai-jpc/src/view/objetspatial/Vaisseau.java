package view.objetspatial;

public final class Vaisseau extends ObjetSpatial{
	private static int vie;
	private boolean mouvementGauche;
	private boolean mouvementDroite;
	private boolean mouvementHaut;

	public Vaisseau(int unePosX, int unePosY, int uneVitesse) {
		super(unePosX, unePosY, uneVitesse, 0, "/image/vaisseau.png");
		Vaisseau.vie = 5;
		this.mouvementDroite = false;
		this.mouvementGauche = false;
		this.mouvementHaut = false;
	}

	public static int getVie() {
		return vie;
	}

	public static void setVie(int vie) {
		Vaisseau.vie = vie;
	}

	public boolean isMouvementGauche() {
		return mouvementGauche;
	}

	public void setMouvementGauche(boolean mouvementGauche) {
		this.mouvementGauche = mouvementGauche;
	}

	public boolean isMouvementDroite() {
		return mouvementDroite;
	}

	public void setMouvementDroite(boolean mouvementDroite) {
		this.mouvementDroite = mouvementDroite;
	}

	public boolean isMouvementHaut() {
		return mouvementHaut;
	}

	public void setMouvementHaut(boolean mouvementHaut) {
		this.mouvementHaut = mouvementHaut;
	}
}

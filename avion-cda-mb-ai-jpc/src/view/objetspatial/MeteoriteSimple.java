package view.objetspatial;

import models.Models;

public final class MeteoriteSimple extends ObjetSpatial{	
	public MeteoriteSimple(int unePosX, int unePosY, int uneVitesse) {
		super(unePosX, unePosY, uneVitesse, Models.getPointMeteoriteSimple(), "/image/meteorite_simple.png");
	}
}

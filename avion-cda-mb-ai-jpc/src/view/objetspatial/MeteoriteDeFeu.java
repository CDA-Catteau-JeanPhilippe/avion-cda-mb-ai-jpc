
package view.objetspatial;

import java.util.Random;

import models.Models;

public final class MeteoriteDeFeu extends ObjetSpatial{
	
	public MeteoriteDeFeu(int unePosX, int unePosY, int uneVitesse) {
		super(unePosX, unePosY, uneVitesse, Models.getPointMeteoriteDeFeu(), "/image/meteorite_feu.png");
	}

	public void bouge() {
		this.getMonImage();
		Random r = new Random();
		this.setPosX(r.nextInt(Models.getLargeurDuPanneau()));
		this.setPosY(this.getPosY() + 50);
		
		if (this.getPosY() > Models.getHauteurDuPanneau()) {
			this.setPosX(r.nextInt(1280 - 200));
		}
	}
}
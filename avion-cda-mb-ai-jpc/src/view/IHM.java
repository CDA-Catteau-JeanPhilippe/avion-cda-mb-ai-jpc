package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;

import models.Models;
import view.panel.BarreDInformation;
import view.panel.FenetreCreationPartie;
import view.panel.PanelPrincipal;

public class IHM extends JFrame {
	private static final long serialVersionUID = 1L;
	private BarreDInformation barreDInformation;
	private PanelPrincipal panelPrincipal;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new IHM();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IHM() {
		new FenetreCreationPartie();
		panelPrincipal = new PanelPrincipal();
		barreDInformation = new BarreDInformation();
		this.add(barreDInformation.getBarreDInformation(), BorderLayout.NORTH); // Ajoute la barre d'information
		this.add(panelPrincipal); // Ajoute la partie jeu
		this.setTitle("CDA-Avion-SWING-Games"); // Donne un titre � la fen�tre
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(Models.getLargeurDuPanneau(), Models.getHauteurDuPanneau()); // D�finition sa taille largeur hauteur
		this.setLocationRelativeTo(null); // centre la fenetre par rapport au bureau
		this.setResizable(false); // bloque le redimensionnement
		this.setVisible(true);
	}
}

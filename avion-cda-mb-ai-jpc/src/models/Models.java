package models;

public class Models {
	private static final int LARGEUR_DU_PANNEAU = 1080;
	private static final int HAUTEUR_DU_PANNEAU = 860;
	private static final int POSITION_X_DU_VAISSEAU_AU_DEPART = 475;
	private static final int POSITION_Y_DU_VAISSEAU_AU_DEPART = 600;
	private static final int POSITION_X_DE_LA_METEORITE_DE_FEU_AU_DEPART = 200;
	private static final int POSITION_Y_DE_LA_METEORITE_DE_FEU_AU_DEPART = 0;
	private static final int POINT_METEORITE_SIMPLE = 2;
	private static final int POINT_METEORITE_DE_FEU = 1;
	private static final int POINT_METEORITE_DE_GLACE = 3;
	private static final int POINT_METEORITE_ZIGZAG = 5;
	private static final int POINT_METEORITE_ICEBERG = 5;
	
	public static int getLargeurDuPanneau() {
		return LARGEUR_DU_PANNEAU;
	}
	public static int getHauteurDuPanneau() {
		return HAUTEUR_DU_PANNEAU;
	}
	public static int getPositionXDuVaisseauAuDepart() {
		return POSITION_X_DU_VAISSEAU_AU_DEPART;
	}
	public static int getPositionYDuVaisseauAuDepart() {
		return POSITION_Y_DU_VAISSEAU_AU_DEPART;
	}
	public static int getPositionXDeLaMeteoriteDeFeuAuDepart() {
		return POSITION_X_DE_LA_METEORITE_DE_FEU_AU_DEPART;
	}
	public static int getPositionYDeLaMeteoriteDeFeuAuDepart() {
		return POSITION_Y_DE_LA_METEORITE_DE_FEU_AU_DEPART;
	}
	public static int getPointMeteoriteSimple() {
		return POINT_METEORITE_SIMPLE;
	}
	public static int getPointMeteoriteDeFeu() {
		return POINT_METEORITE_DE_FEU;
	}
	public static int getPointMeteoriteDeGlace() {
		return POINT_METEORITE_DE_GLACE;
	}
	public static int getPointMeteoriteZigzag() {
		return POINT_METEORITE_ZIGZAG;
	}
	public static int getPointMeteoriteIceberg() {
		return POINT_METEORITE_ICEBERG;
	}
}
